<!-- User Modal -->
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <ul class="nav nav-tabs justify-content-center" id="userTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true">Logowanie</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="false">Rejestracja</a>
          </li>
        </ul>
        <div class="tab-content" id="userTabContent">
          <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
            <h4 class="text-center mt-3">Logowanie do serwisu</h4>
            <form method="POST" action="login">
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
              <div class="form-group">
                <label for="login-email"><i class="fa fa-envelope"></i> E-mail</label>
                <input type="email" name="lEmail" class="form-control" id="login-email">
              </div>
              <div class="form-group">
                <label for="login-password"><i class="fa fa-lock"></i> Hasło</label>
                <input type="password" name="lPassword" id="login-password" class="form-control">
              </div>
              <div class="form-group justify-content-center">
                <?php echo $this->recaptcha->getWidget(); ?>
              </div>
              <div class="form-group">
                <p class="text-center">By signing up you accept our <a href="#">Terms Of Use</a></p>
              </div>
              <div class="col-md-12 text-center ">
                <input type="submit" class=" btn btn-block mybtn btn-primary tx-tfm" name="lSubmit" value="Zaloguj się" />
              </div>
              <div class="col-md-12 ">
                <div class="login-or text-center">
                  <hr class="hr-or">
                  <span class="span-or">lub</span>
                </div>
              </div>
              <div class="col-md-12 mb-3">
                
              </div>
              <div class="form-group">
                <p class="text-center">Nie posiadasz konta? <a href="#register" id="signup" role="tab" data-toggle="tab" aria-controls="register" aria-selected="false">Zarejestruj się!</a></p>
              </div>

            </form>
          </div>
          <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
            <h4 class="text-center mt-3">Rejestracja w serwisie</h4>
            <form method="POST" action="register">
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
              <div class="form-group">
                <label for="register-name"><i class="fa fa-user"></i> Nazwa użytkownika</label>
                <input type="text" name="rName" class="form-control" id="register-name">
              </div>
              <div class="form-group">
                <label for="register-email"><i class="fa fa-envelope"></i> E-mail</label>
                <input type="email" name="rEmail" class="form-control" id="register-email">
              </div>
              <div class="form-group">
                <label for="register-password"><i class="fa fa-lock"></i> Hasło</label>
                <input type="password" name="rPassword" id="register-password" class="form-control">
              </div>
              <div class="form-group justify-content-center">
                <?php echo $this->recaptcha->getWidget(); ?>
              </div>
              <div class="form-group">
                <p class="text-center">By signing up you accept our <a href="#">Terms Of Use</a></p>
              </div>
              <div class="col-md-12 text-center ">
                <input type="submit" class=" btn btn-block mybtn btn-warning tx-tfm" name="rSubmit" id="m-l-submit" value="Zarejestruj się" />
              </div>
              <div class="col-md-12 ">
                <div class="login-or text-center">
                  <hr class="hr-or">
                  <span class="span-or">lub</span>
                </div>
              </div>
              <div class="col-md-12 mb-3">
                
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>