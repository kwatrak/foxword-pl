<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="globalNav">
    <div class="navbar-collapse collapse w-100 dual-collapse2 order-1 order-md-0">
        <ul class="navbar-nav ml-auto text-center" style="font-size: 1.5em;">
            <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="tooltip" data-placement="bottom" title="Home"><i class="fa fa-home"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="tooltip" data-placement="bottom" title="Słownik"><i class="fa fa-book"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="tooltip" data-placement="bottom" title="Tłumacz"><i class="fa fa-language"></i></a>
            </li>
        </ul>
    </div>
    <div class="mx-auto my-2 order-0 order-md-1 position-relative">
        <a class="mx-auto" href="/">
            <img src="<?php echo base_url('public/images/logo.png'); ?>" width="120" height="120" class="rounded-circle">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 dual-collapse2 order-2 order-md-2">
        <ul class="navbar-nav mr-auto text-center" style="font-size: 1.5em;">
            <li class="nav-item">
                <?php if (!$this->auth_model->isLogged()): ?>
                    <span data-toggle="modal" data-target="#userModal">
                        <a class="nav-link" href="#" data-toggle="tooltip" data-placement="bottom" title="Panel Użytkownika"><i class="fa fa-user"></i></a>
                    </span>
                <?php else: ?>
                    <a class="nav-link" href="#" data-toggle="tooltip" data-placement="bottom" title="Witaj <?php echo $this->user_model->getData()->username; ?>!"><i class="fa fa-user"></i></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="tooltip" data-placement="bottom" title="Pomoc"><i class="fa fa-support"></i></a>
            </li>
        </ul>
    </div>
</nav>