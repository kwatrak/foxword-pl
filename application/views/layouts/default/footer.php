	</div>

	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/js/bootstrap.min.js" integrity="sha384-3qaqj0lc6sV/qpzrc1N5DC6i1VRn/HyX4qdPaiEFbn54VjQBEU341pvjz7Dv3n6P" crossorigin="anonymous"></script>
	<script src="https://kit.fontawesome.com/447adf8141.js" crossorigin="anonymous"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	<?php echo $this->recaptcha->getScriptTag(); ?>
	<script src="<?php echo base_url('public/js/sticky.js'); ?>"></script>
	<script src="<?php echo base_url('public/js/main.js'); ?>"></script>
	<?php if ($this->session->flashdata("error") != null): ?>
		<script type="text/javascript"> toastr.error(`<?php echo $this->session->flashdata("error"); ?>`, 'Błąd!'); </script>
	<?php endif; ?>
	<?php if ($this->session->flashdata("success") != null): ?>
		<script type="text/javascript"> toastr.success(`<?php echo $this->session->flashdata("success"); ?>`, 'Sukces!'); </script>
	<?php endif; ?>

	<?php $this->load->view('layouts/default/modals.php'); ?>

</body>
</html>