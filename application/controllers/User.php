<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->auth_model->isLogged()) {
			return redirect('/');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('id');
		$this->session->set_flashdata('success', "Wylogowano!");
		return redirect('/');
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */