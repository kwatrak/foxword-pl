<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index() {
		$this->load->view("layouts/default/header.php");
		$this->load->view("layouts/default/footer.php");
	}

	public function register() {
		if ($this->input->post('rSubmit') == null) {
			return show_error(500, "Invalid POST request");
		}

		$this->form_validation->set_rules('rName', 'Nazwa użytkownika', 'required|min_length[3]|max_length[25]|is_unique[users.username]');
		$this->form_validation->set_rules('rEmail', 'E-mail', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('rPassword', 'Hasło', 'required|min_length[6]|max_length[20]');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			return redirect($_SERVER['HTTP_REFERER']);
		}

		$recaptcha = $this->input->post('g-recaptcha-response');
		$response = $this->recaptcha->verifyResponse($recaptcha);

		if (!(isset($response['success']) && $response['success'] === true)) {
			$this->session->set_flashdata('error', "Musisz zweryfikować się za pomocą ReCaptcha!");
			return redirect($_SERVER['HTTP_REFERER']);
		}
		
		$password = password_hash($this->input->post('rPassword'), PASSWORD_BCRYPT);
		$uid = $this->auth_model->insert(array(
			'username' => $this->input->post('rName'),
			'email' => $this->input->post('rEmail'),
			'password' => $password,
			'permissions' => 'USER'
		));
		$sended = $this->auth_model->sendToken($this->input->post('rEmail'), $uid);

		if ($sended) {
			$this->session->set_flashdata('success', "Konto zostało zarejestrowane! Aktywuj swoje konto klikając w link, który wysłaliśmy Ci na adres " . $this->input->post('rEmail'));
			return redirect('/');
		}

		$this->session->set_flashdata('error', "Twoje konto zostało założone, ale nie mogliśmy wysłać kodu aktywacyjnego. W celu aktywacji konta skontaktuj się z administratorem.");
		return redirect('/');
	}

	public function login() {
		if ($this->input->post('lSubmit') == null) {
			return show_error(500, "Invalid POST request");
		}

		$this->form_validation->set_rules('lEmail', 'E-mail', 'required|valid_email');
		$this->form_validation->set_rules('lPassword', 'Hasło', 'required|min_length[6]|max_length[20]');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			return redirect($_SERVER['HTTP_REFERER']);
		}

		$recaptcha = $this->input->post('g-recaptcha-response');
		$response = $this->recaptcha->verifyResponse($recaptcha);

		if (!(isset($response['success']) && $response['success'] === true)) {
			$this->session->set_flashdata('error', "Musisz zweryfikować się za pomocą ReCaptcha!");
			return redirect($_SERVER['HTTP_REFERER']);
		}

		$user = $this->auth_model->authenticate($this->input->post('lEmail'), $this->input->post('lPassword'));

		if ($user == null) {
			$this->session->set_flashdata('error', "Użytkownik o takich danych nie istnieje, podano nieprawidłowe dane lub konto nie zostało aktywowane!" . $user);
			return redirect($_SERVER['HTTP_REFERER']);
		}

		$this->session->set_userdata(array('id' => $user->id));
		$this->session->set_flashdata('success', "Zalogowano!");
		return redirect('/');
	}

	public function verify($token) {
		$activated = $this->auth_model->active($token);
		if ($activated) {
			$this->session->set_flashdata('success', "Twoje konto zostało aktywowane!");
			return redirect('/');
		}

		$this->session->set_flashdata('error', "Nieprawidłowy token aktywacji!");
		return redirect('/');
	}

}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */