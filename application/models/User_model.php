<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function getData() {
		$this->db->select();
		$this->db->from('users');
		$this->db->where('id', $this->session->userdata('id'));
		$query = $this->db->get();
		$result = $query->result();

		return $result[0];


	}

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */