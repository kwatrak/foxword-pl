<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function insert($data = array()) {
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}

	public function sendToken($email = "", $user = 0) {
		$token = random_string('md5', 16);
		$this->db->insert('verify', array(
			'user' => $user,
			'token' => $token
		));

		$config = array(
		    'protocol'  => 'smtp',
		    'smtp_host' => 'ssl://smtp.googlemail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'konradwatrak@gmail.com',
		    'smtp_pass' => '######',
		    'mailtype'  => 'html',
		    'charset'   => 'utf-8'
		);
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		$result = $this->email
			->from('konradwatrak@gmail.com')
			->reply_to('konradwatrak@gmail.com')
			->to($email)
			->subject('FoxWord Aktywacja')
			->message('Aby potwierdzić swoje konto, kliknij w ten link: <a href="'.base_url('verify/' . $token).'">Aktywacja</a>.<br>Jeśli link nie działa, wklej ten: ' . base_url('verify/' . $token))
			->send();

		if ($result)
			return true;

		return false;
	}

	public function active($token = "") {
		$this->db->select();
		$this->db->from('verify');
		$this->db->where('token', $token);
		$query = $this->db->get();
		$result = $query->result();

		if (!$result) {
			return false;
		}

		$this->db->delete('verify', array('token' => $token));

		return true;
	}

	public function authenticate($mail = "", $password = "") {
		$this->db->select();
		$this->db->from("users");
		$this->db->where('email', $mail);
		$query = $this->db->get();
		$result = $query->result();

		if (!($result)) {
			return null;
		}

		$udata = $result[0];

		if (password_verify($password, $udata->password)) {
			if (!$this->isActivated($udata->id)) {
				return null;
			}

			return $udata;
		}

		return null;
	}

	public function isActivated($uid = 0) {
		$this->db->select();
		$this->db->from('verify');
		$this->db->where('user', $uid);
		$query = $this->db->get();
		$result = $query->result();

		if ($result) {
			return false;
		}

		return true;
	}

	public function isLogged() {
		if (!$this->session->has_userdata('id')) {
			return false;
		}

		return true;
	}

}

/* End of file Auth.php */
/* Location: ./application/models/Auth.php */